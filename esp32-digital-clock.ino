#include <LiquidCrystal_I2C.h>
#include <WiFi.h>
#include <ESP32Console.h>
#include <Preferences.h>
#include <nvs_flash.h>
#include "time.h"
using namespace ESP32Console;
Console console;
Preferences prefs;

static void set_timezone();                 // Declare set_timezone function
static long get_gmt_offset(void);            // Declare get_gmt_offset function

const long gmt_offset_sec = get_gmt_offset();  // gmt_offset_sec from get_gmt_offset
const char *ntp_server = "pool.ntp.org";    // ntp server
const int daylight_offset_sec = 0;          // configTime() expects this but get_gmt_offset already provides it.

// set the LCD number of columns and rows
const int lcd_columns = 16;
const int lcd_rows = 2;

// timeinfo struct
struct tm timeinfo;

// set LCD i2c address (GPIO pins G21 and G22 for me), and number of columns and rows
LiquidCrystal_I2C lcd(0x3F, lcd_columns, lcd_rows);

// wifi command
int setup_wifi(int argc, char **argv) {
  // Ensure we have an argument to parse
  if (argc != 3) {
    printf("You need to give your wifi ssid and password.\n");
    // Return exit_failure if nothing worked
    return EXIT_FAILURE;
  }

  prefs.begin("credentials", false);
  prefs.putString("ssid", argv[1]);
  prefs.putString("pass", argv[2]);
  prefs.end();
  return EXIT_SUCCESS;
}

// erase nvram command
int erase_nvram(int argc, char **argv) {
  nvs_flash_erase();  // Erase the NVS partition
  nvs_flash_init();   // Initialize the NVS partition.
  return EXIT_SUCCESS;
}

// timezone command
int timezone(int argc, char **argv) {
  if (argc != 2) {
    printf("You need to give a timezone.\n");
    return EXIT_FAILURE;
  }

  prefs.begin("timezone", false);
  prefs.putString("tzinfo", argv[1]);
  prefs.end();
  Serial.println("Timezone set, please restart");
  return EXIT_SUCCESS;
}

// shorttime command
int shorttime(int argc, char **argv) {
  if (argc != 2) {
    Serial.println("You need to give a true/false argument!");
    return EXIT_FAILURE;
  }

  prefs.begin("shorttime", false);

  if (strcmp(argv[1], "true") == 0) {
    prefs.putBool("shorttime", true);
    Serial.println("Short time set to true, please restart!");
    return EXIT_SUCCESS;
  } else if (strcmp(argv[1], "false") == 0) {
    prefs.putBool("shorttime", false);
    Serial.println("Short time set to false, please restart!");
    return EXIT_SUCCESS;
  }

  Serial.println("What?");
  return 0;
}

long get_gmt_offset(void) {
  time_t t, t2;
  struct tm *tm2;
  time(&t);  // Get seconds since 1970
  tm2 = gmtime(&t);
  tm2->tm_isdst = -1;  // Check for daylight savings time and add offset
  t2 = mktime(tm2);    // Convert back to seconds
  return ((t - t2) / 3600);
}

void set_timezone() {
  String timezone;
  // Set timezone env variable based on value set in nvram
  prefs.begin("timezone", false);
  timezone = prefs.getString("tzinfo", "");
  unsetenv("TZ");
  setenv("TZ", timezone.c_str(), 1);
  tzset();
}

bool get_short_time() {
  prefs.begin("shorttime");
  bool shorttime = prefs.getBool("shorttime", "");
  return shorttime;
}

void setup_serial() {
  Serial.begin(115200);
  // Experimental serial console control
  Serial.println("Manually set date and time with: date -s \"YYYY-MM-DD HH:MM:SS\"");
  Serial.println("Set WiFi credentials with: wifi");
  Serial.println("Erase nvram with: erase_nvram");
  console.begin(115200);
  console.setPrompt("DigitalClock>> ");
  console.registerSystemCommands();
  console.registerCommand(ConsoleCommand("erase_nvram", &erase_nvram, "Erase the NVRAM"));
  console.registerCommand(ConsoleCommand("wifi", &setup_wifi, "Connect to WiFi"));
  console.registerCommand(ConsoleCommand("timezone", &timezone, "Set the timezone"));
  console.registerCommand(ConsoleCommand("shorttime", &shorttime, "Set whether or not to display the time in short time (12h) or long time (24h)."));
}

void connect_to_wifi() {
  prefs.begin("wifi");
  Serial.print("Connecting to ");

  prefs.begin("credentials", false);

  String ssid;
  String pass;
  ssid = prefs.getString("ssid", "");
  pass = prefs.getString("pass", "");
  if (ssid == "" || pass == "") {
    Serial.println("No WiFi credentials saved, please use wifi command in the serial console to connect!");
  } else {
    Serial.println(ssid);
    WiFi.begin(ssid.c_str(), pass.c_str());
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected.");
  }
}

void print_local_time() {
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
  }

  configTime(gmt_offset_sec, daylight_offset_sec, ntp_server);

  bool shorttime = get_short_time();

  if (shorttime == true) {
    lcd.setCursor(0, 0);
    lcd.print(&timeinfo, "%I:%M:%S %p");
  } else {
    lcd.setCursor(0, 0);
    lcd.print(&timeinfo, "%H:%M:%S");
  }
  lcd.setCursor(0, 1);
  lcd.print(&timeinfo, "%m/%d/%Y");
}

void setup() {
  // Setup serial
  setup_serial();
  
  // Connect to WiFi
  connect_to_wifi();

  // Init the LCD screen
  lcd.init();
  lcd.backlight();
}

void loop() {
  // Set the timezone
  set_timezone();

  // Print the time on the LCD screen
  print_local_time();
}
